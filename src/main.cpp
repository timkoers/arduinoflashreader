#include <Arduino.h>

#include <digitalWriteFast.h>

constexpr size_t ADDRESS_LINES = 19; // 0-18
constexpr size_t DATA_LINES    = 16; // 0-15

const uint8_t address_pins[ADDRESS_LINES] = { 0 };
const uint8_t data_pins   [DATA_LINES]    = { 0 };

constexpr uint8_t nCE = 0;   // Chip enable
constexpr uint8_t nOE = 0;   // Output enable
constexpr uint8_t nWE = 0;   // Write enable
constexpr uint8_t nRP = 0;   // Reset/power down
constexpr uint8_t nWP = 0;   // Write protect
constexpr uint8_t nBYTE = 0; // Toggles between 8/16-bit mode

typedef union{
  bool raw[32];
  uint32_t address;
} address_data;

typedef union{
  bool raw[16];
  uint16_t data;
} data_data;

// Sets the address to the output pins
void setAddress(uint32_t address){
  address_data addr;
  addr.address = address;

  for(size_t idx = 0; idx < ADDRESS_LINES; idx+= 2){
    digitalWriteFast(address_pins[idx], addr.raw[idx]);
  }
}

// Gets the data from the input pins
uint16_t getData(){
  data_data data;

  for(size_t idx = 0; idx < DATA_LINES; idx++){
    data.raw[idx] = digitalReadFast(data_pins[idx]);
  }

  return data.data;
}

void readIdentifier(){
  digitalWriteFast(nOE, HIGH); // Disable the output
  digitalWriteFast(nWP, LOW);  // Make sure to keep write protect enabled
  digitalWriteFast(nWE, HIGH); // Make sure to enter command register mode

  // Enter intelligent identifier mode
  for(size_t idx = 0; idx < 8; idx++){
    pinModeFast(data_pins[idx], OUTPUT);
    digitalWriteFast(data_pins[idx], LOW);
  }

  // Write the 90
  digitalWriteFast(data_pins[7], HIGH);
  digitalWriteFast(data_pins[4], HIGH);

  // Enable the chip
  digitalWriteFast(nCE, LOW);

  delayMicroseconds(10);

  digitalWriteFast(nCE, HIGH);

  digitalWriteFast(data_pins[7], LOW);
  digitalWriteFast(data_pins[4], LOW);

  // Enter read mode and read the manufacturer id and device id
  digitalWriteFast(nOE, LOW);

  // Set data pins as input
  for(size_t idx = 0; idx < 8; idx++){
    pinModeFast(data_pins[idx], INPUT);
  }

  digitalWriteFast(address_pins[0], LOW); // Manufacturer id
  digitalWriteFast(nCE, LOW); // Enable the chip

  uint16_t manId = getData();

  digitalWriteFast(nCE, HIGH);

  digitalWriteFast(address_pins[0], HIGH); // Device id
  digitalWriteFast(nCE, LOW); // Enable the chip

  uint16_t devId = getData();

  digitalWriteFast(nCE, HIGH); // Disable the chip

  Serial.print("Manufacturer: ");
  Serial.println(manId, HEX);
  Serial.print("Device: ");
  Serial.println(devId, HEX);
}

void setup() {

  Serial.begin(256000);
  Serial2.begin(115200);

  Serial2.println("[INFO]: Setting pin modes");

  // Setup the address lines
  for(size_t pin = 0; pin < ADDRESS_LINES; pin++){
    pinModeFast(address_pins[pin], OUTPUT);
  }

  // Setup the data lines
  for(size_t pin = 0; pin < DATA_LINES; pin++){
    pinModeFast(data_pins[pin], INPUT);
  }

  pinModeFast(nCE,   OUTPUT);
  pinModeFast(nOE,   OUTPUT);
  pinModeFast(nWE,   OUTPUT);
  pinModeFast(nRP,   OUTPUT);
  pinModeFast(nWP,   OUTPUT);
  pinModeFast(nBYTE, OUTPUT);

  Serial2.println("[INFO]: Enabling chip");

  // Enter read mode
  digitalWriteFast(nOE,   LOW );
  digitalWriteFast(nWE,   HIGH);
  digitalWriteFast(nRP,   HIGH);
  digitalWriteFast(nWP,   LOW );
  digitalWriteFast(nBYTE, HIGH); // Enable 16-bit mode

}

void loop() {
  Serial2.println("[INFO]: Waiting for clear to receive..");

  // Wait until clear to receive is received
  while(Serial.readString() != "CTR"){
    delay(1);
  }

  readIdentifier();

  // Read out the entire flash memory and dump contents to UART
  // The number of available addresses is devided by 2, due to the word size read mode
  for(uint32_t address = 0; address < ((2^ADDRESS_LINES)-1/2); address++){

    Serial2.print("[READ]: Reading@");
    Serial2.print(address, HEX);
    Serial2.print(": ");

    setAddress(address);

    digitalWriteFast(nCE,   LOW ); // Enable the chip

    uint16_t data = getData();
    Serial.write(data);

    Serial2.println(data, HEX);

    digitalWriteFast(nCE,   HIGH ); // Disable the chip
  }
}