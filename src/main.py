from prompt_toolkit.shortcuts import ProgressBar
import serial
import argparse
import sys

ADDRESS_WIDTH = 19
FILE_LENGTH = ((2^19)-1)/2

if __name__ == '__main__':

    # Parse arguments
    parser = argparse.ArgumentParser(description="ArduinoFlashReader host script")
    parser.add_argument('--file', required=False           , help="Output file name"  , default="flash.bin")
    parser.add_argument('--port', required=False           , help="Serial port to use", default="COM3"     )
    parser.add_argument('--baud', required=False, type=int , help="Baudrate to use"   , default=256000     )

    args = parser.parse_args(sys.argv[1:])

    # Open the serial port
    with serial.Serial(args.port, args.baud, timeout =1) as ser:

      if ser.is_open:
        print(f"[INFO]: Opened {args.port}@{args.baud}")
      else:
        print(f"[ERROR]: Failed to open {args.port}@{args.baud}")

      # Write clear to receive
      ser.write(b'CTR')

      # Receive the manufacturer and device id's
      CHIP_DATA = ser.readline()

      with open(args.file, "wb") as file:
        with ProgressBar() as pb:
          for addr in pb(range(FILE_LENGTH), f"Reading {CHIP_DATA}"):

            file.write(ser.read(2))

          file.flush()
